#!/bin/bash

# Converts the given Org-mode file to HTML and posts it to local.simpsonian.ca.

TARGET="josh@treebeard.local"
HTML_ROOT="/var/www/html"

INPUT_FILE=$1
TEMP_FILE=$(mktemp)
OUTPUT_FILE="${INPUT_FILE%.org}.html"

SCP_TARGET="${TARGET}:${HTML_ROOT}/org/${OUTPUT_FILE}"
OUTPUT_URL="https://local.simpsonian.ca/org/${OUTPUT_FILE}"

# Note: HTML5 requires a non-empty <title> element, but in pandoc, setting the
# `title` metadata also adds the title as in a <h1> to the start of the
# document. We don't want that--setting `pagetitle` instead does the former
# without the latter.
pandoc --standalone \
    -f org -t html \
    --metadata pagetitle="${INPUT_FILE%.org}" \
    -o "$TEMP_FILE" \
    "$INPUT_FILE"
# Ensure the file is globally readable to avoid permissions issues when served
# by Apache.
chmod a+r "$TEMP_FILE"
# -p: preserve permissions; -q: quiet
scp -pq "$TEMP_FILE" "$SCP_TARGET"

echo "Published to ${OUTPUT_URL}."
